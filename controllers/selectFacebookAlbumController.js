(function() {
'use strict';

var controllerId = 'stormideas.selectImageFromFacebook.selectFacebookAlbum';

angular.module('stormideas.selectImageFromFacebook').controller(controllerId, ['$location', '$rootScope', '$scope', 'connectService', 'facebookAlbums', selectFacebookAlbum]);

function selectFacebookAlbum($location, $rootScope, $scope, connectService, facebookAlbums) {
    $scope.facebookAlbums = facebookAlbums;
    $scope.facebookAlbumTemplate = '<div ng-style="{\'background-image\': \'url(https://graph.facebook.com/\' + getGridItem(i,j).id + \'/picture?type=album&access_token=' + connectService.getAccessToken() + ')\'}" style="background-position: 50% 25%; background-size: cover; padding-bottom: 100%; cursor: pointer;"></div><p>{{getGridItem(i,j).name}}</p>';

    $scope.selectFacebookAlbum = function(selectedAlbum) {
        // TODO : Tracking
        // TODO : Loading
        $location.path("/selectFacebookImage/" + selectedAlbum.id);
    }; 
}
})();