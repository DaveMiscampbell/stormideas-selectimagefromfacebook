(function() {
'use strict';

var controllerId = 'stormideas.selectImageFromFacebook.selectImageFromFacebook';

angular.module('stormideas.selectImageFromFacebook').controller(controllerId, ['$location', '$rootScope', '$scope', 'facebookImages', 'stormideas.selectImageFromFacebook.selectFacebookImageDelegate', selectImageFromFacebook]);

function selectImageFromFacebook($location, $rootScope, $scope, facebookImages, selectFacebookImageDelegate) {
    $scope.facebookImages = facebookImages;
    $scope.facebookImageTemplate = '<div ng-style="{\'background-image\': \'url(\' + getGridItem(i,j).source + \')\'}" style="background-position: 50% 25%; background-size: cover; padding-bottom: 100%; cursor: pointer;"></div>';

    $scope.selectFacebookImage = function(selectedImage) {
        selectFacebookImageDelegate.onSelectImageFromFacebook(selectedImage);
    };
}

})();