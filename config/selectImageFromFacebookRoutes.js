(function () {
    'use strict';

    angular.module('stormideas.selectImageFromFacebook').config(['$routeProvider', 'stormideas.selectImageFromFacebook.settings', routeConfigurator]);

    function routeConfigurator($routeProvider, settings) {
        $routeProvider.when('/selectFacebookAlbum', {
            templateUrl: settings.facebookAlbumTemplateUrl || '/app/main/views/selectFacebookAlbum.html',
            controller: 'stormideas.selectImageFromFacebook.selectFacebookAlbum',
            resolve: {
                'facebookAlbums': ['stormideas.selectImageFromFacebook.facebookImageService', function (facebookImageService) {
                    return facebookImageService.getFacebookAlbums();
                }]
            },
            caseInsensitiveMatch: true
        });

        $routeProvider.when('/selectFacebookImage/:facebookAlbum', {
            templateUrl: settings.facebookImageTemplateUrl || '/app/main/views/selectFacebookImage.html',
            controller: 'stormideas.selectImageFromFacebook.selectImageFromFacebook',
            resolve: {
                'facebookImages': ['$route', 'stormideas.selectImageFromFacebook.facebookImageService', function ($route, facebookImageService) {
                    return facebookImageService.getFacebookImages($route.current.params.facebookAlbum);
                }]
            },
            caseInsensitiveMatch: true
        });
    }

})();