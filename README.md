# StormIdeas - Select Image From Facebook Module

This module is designed to make it easy to select an image from a user's Facebook photos for use in an Angular Application. It makes a lot of assumptions!

# Configuration

To customize the views for the two screens, create an angular constant as follows :

    (function() {
    	'use strict';
    
    	var selectImageFromFacebookSettings = {
    		'facebookAlbumTemplateUrl': '/app/main/views/selectFacebookAlbum.html',
    		'facebookImageTemplateUrl': '/app/main/views/selectImageFromFacebook.html'
	    };
    
	    angular.module('stormideas.selectImageFromFacebook').constant('stormideas.selectImageFromFacebook.settings', selectImageFromFacebookSettings);

    })();

When an image is selected from Facebook, the module will look for a delegate and pass the selectedImage to it. Create your delegate as follows :

    (function() {
    	'use strict';
    
    	var serviceId = 'stormideas.selectImageFromFacebook.selectFacebookImageDelegate';
    
    	angular.module('main').factory(serviceId, [selectFacebookImageDelegate]);
    
    	function selectFacebookImageDelegate() {
    		var service = {
    			onSelectImageFromFacebook: onSelectImageFromFacebook
    		};
    
    		return service;
    
    		function onSelectImageFromFacebook(selectedImage) {
    			console.log(selectedImage.source); // Selected Image Url
    		});
    	}

    })();

