var gulp = require('gulp');
var jshint = require('gulp-jshint');

var jsPaths = [
    './config/*.js', 
    './controllers/*.js', 
    './directives/*.js', 
    './providers/*.js',
    './services/*.js', 
    'selectImageFromFacebook.js'
];

gulp.task('lint', function(taskDone) {
    return gulp.src(jsPaths)
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('default', ['lint']);

gulp.task('watch', function() {
    gulp.watch(jsPaths, ['lint']);
});