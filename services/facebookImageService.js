(function() {
    'use strict';

    var serviceId = 'stormideas.selectImageFromFacebook.facebookImageService';

    angular.module('stormideas.selectImageFromFacebook').factory(serviceId, ['$q', 'connectService', 'facebookService', facebookImageService]);

    function facebookImageService($q, connectService, facebookService) {

        function getFacebookAlbums() {
            return facebookService.queryApi('/me/albums', { limit: 100, fields: 'count' }).then(function(data) {
                var populatedAlbums = data.filter(function(x) { return x.count; });
                return $q.when(populatedAlbums);
            });
        }

        function getFacebookImages(selectedAlbumId) {
            return facebookService.queryApi('/' + selectedAlbumId + '/photos', { limit: 100, fields: 'source' });
        }

        var service = { 
            getFacebookAlbums: getFacebookAlbums,
            getFacebookImages: getFacebookImages
        };

        return service;
    }
})();